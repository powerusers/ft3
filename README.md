# ft3

How to setup a membrane keyboard.

A simple example of how to use a membrane keyboard with Python on a Raspberry Pi.

### Prerequisites

No prerequisites on a Raspberry Pi based on Raspbian -
Tested with February version (2020-02-13) of Raspbian Buster with Desktop.

### Components

The component is a 3 button membrane keyboard with 4 pins.

https://www.segor.de/#Q=FT3rt%252Fge%252Fgn&M=1

### Tags
rpi, rasperry pi, python, membrane keyboard, Folientastatur, 3-Tasten, Taster, Button
