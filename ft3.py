#!/usr/bin/python
# ft3.py

import RPi.GPIO as GPIO
import time
import os

P_RED    = 23 
P_YELLOW = 21
P_GREEN  = 19
 
def setup():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(P_RED, GPIO.IN, GPIO.PUD_DOWN)
    GPIO.setup(P_YELLOW, GPIO.IN, GPIO.PUD_DOWN)
    GPIO.setup(P_GREEN, GPIO.IN, GPIO.PUD_DOWN)

setup()

while True:
    if GPIO.input(P_RED) == GPIO.HIGH:
        print ("Red button");
        time.sleep(0.5);
    if GPIO.input(P_YELLOW) == GPIO.HIGH:
        print ("Yellow button");
        time.sleep(0.5);
    if GPIO.input(P_GREEN) == GPIO.HIGH:
        print ("Green button");
        time.sleep(0.5);
    time.sleep(0.2);
